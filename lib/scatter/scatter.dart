/// Contain a widget that displays a collection of dispersed and non-overlapping children
library scatter;

export 'package:the_nipple_s_hiding_place/scatter/src/rendering/scatter.dart';
export 'package:the_nipple_s_hiding_place/scatter/src/widgets/scatter.dart';
