import 'dart:typed_data';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';

class FirebaseManager {
  FirebaseManager() {
    Firebase.initializeApp();
  }

  Future<void> uploadPic(Uint8List image) async {
    String filename = DateTime.now().millisecondsSinceEpoch.toString();
    await FirebaseStorage.instance.ref('images/$filename.jpg').putData(image);
    print('upload completato');
  }

  Future<void> uploadHQ(Uint8List image) async {
    await FirebaseStorage.instance.ref('HQ/HQ.jpg').putData(image);
    print('upload completato');
  }

  /// Ritorna una lista con i link per le immagini
  Future<List<String>> listaCaricamenti() async {
    FirebaseStorage storage = FirebaseStorage.instance;

    Reference ref = storage.ref('images');

    ListResult result = await ref.listAll();

    List<String> listaDownload = List<String>.empty(growable: true);

    String url;
    for (int a = 0; a < result.items.length; a++) {
      url = await result.items[a].getDownloadURL();
      listaDownload.add(url);
    }
    return listaDownload;
  }

  Future<String> percorsoHQ() async {
    FirebaseStorage storage = FirebaseStorage.instance;

    Reference ref = storage.ref('HQ');
    return await (await ref.listAll()).items.first.getDownloadURL();
  }
}
